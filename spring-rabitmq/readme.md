cmd kafka

- start zookeeper
.\bin\windows\zookeeper-server-start.bat .\config\zookeeper.properties

- start kafka

.\bin\windows\kafka-server-start.bat .\config\server.properties


docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:latest