package vn.com.messagingrabbitmq;

import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import vn.com.receiver.ReceiverManager;

@Configuration
public class Listener {
	private final String queueName = "spring-boot-rabbitmq-response";
	//defined container
	  @Bean
	  SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
	      MessageListenerAdapter listenerAdapter) {
	    SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
	    container.setConnectionFactory(connectionFactory);
	    container.setQueueNames(queueName);
	    container.setMessageListener(listenerAdapter);
	    return container;
	  }
	  
	  //method is registered as a message listener in the container
	  @Bean
	  MessageListenerAdapter listenerAdapter(ReceiverManager receiver) {
	    return new MessageListenerAdapter(receiver, "receiveMessage");
	  }
}
