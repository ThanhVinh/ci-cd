//package vn.com.kafkaconfig;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import org.apache.kafka.clients.admin.AdminClientConfig;
//import org.apache.kafka.clients.admin.NewTopic;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.kafka.core.KafkaAdmin;
//
//@Configuration
//public class KafkaTopicConfig {
//	@Value(value = "${spring.kafka.producer.bootstrap-servers}")
//    private String bootstrapServer;
//	
//	// KafkaAdmin Spring bean, which will automatically add topics for all beans of type NewTopic:
//    @Bean
//    public KafkaAdmin kafkaAdmin() {
//        Map<String, Object> configs = new HashMap<>();
//        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
//        return new KafkaAdmin(configs);
//    }
//    
//    //create new toppic
//    @Bean
//    public NewTopic topic() {
//    	//new NewTopic (String nametopic, numPartition, short replicationFactor)
//    	// bealdung : tên toppic
//    	// 1: số phân vùng của topic bealdung
//    	// (short)1: Hệ số sao chép cho chủ đề mới
//         return new NewTopic("baeldung", 3, (short) 1);
//    }
//}
