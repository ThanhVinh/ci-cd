package vn.com.publisher;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import vn.com.dto.Message;

@RestController
@RequestMapping("api/sendMessage")
public class PublisherController {
	@Autowired
	private RabbitTemplate rabbitTemplate;
	
//	@Autowired
//	private KafkaTemplate<String, String> kafkaTemplate;
	
	ObjectMapper mapper = new ObjectMapper();
	
	@PostMapping("/test")
	public String sendMassage(@RequestBody Message message) throws AmqpException, JsonProcessingException {
		rabbitTemplate.convertAndSend("spring-boot-exchange", "routing.test", mapper.writeValueAsString(message));
		return "success";
	}
	
	@GetMapping("/testServer")
	public String testApi() {
		return "test sucess";
	}
//	@PostMapping("/kafka")
//	public String sendMessage(@RequestBody Message message) throws AmqpException, JsonProcessingException {
//		 kafkaTemplate.send("baeldung", mapper.writeValueAsString(message));
//		return "success";
//	}

}
