package vn.com;


import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringRabitmqApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringRabitmqApplication.class, args);
	}  
	
	@Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setHost(System.getenv("RABBITMQ_HOST"));
        connectionFactory.setVirtualHost(System.getenv("RABBITMQ_VHOST"));
        connectionFactory.setUsername(System.getenv("RABBITMQ_USERNAME"));
        connectionFactory.setPassword(System.getenv("RABBITMQ_PASSWORD"));
        System.out.println("RABBITMQ_HOST "+ System.getenv("RABBITMQ_HOST"));
        return connectionFactory;
    }
}
