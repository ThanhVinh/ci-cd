package vn.com.dto;

import lombok.Data;

@Data
public class Message {
	private String message;
	private String author;
}
