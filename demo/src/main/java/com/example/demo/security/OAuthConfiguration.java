package com.example.demo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

@Configuration
@EnableAuthorizationServer
public class OAuthConfiguration extends AuthorizationServerConfigurerAdapter{
	
	@Autowired
	@Qualifier("authenticationManagerBean")
	private  AuthenticationManager authenticationManager;
	@Autowired
	@Qualifier("clientPasswordEncoder")
	private  PasswordEncoder clientPasswordEncoder;
	
	@Autowired
	private  UserDetailsService userService;
	
	
	
	@Value("${jwt.clientId:client}")
	private String clientId;
	
	@Value("${jwt.client-secret:secret}")
	private String clientSecret;
	
	@Value("${jwt.accessTokenValidititySeconds:43200}")
	private int accessTokenValidititySeconds;
	
	@Value("${jwt.refreshTokenValiditySeconds:2592000}")
	private int refreshTokenValiditySeconds;
	
	@Value("${jwt.authorizedGrantTypes:password,authorization_code,refresh_token}")
	private String[] authorizedGrantTypes;
	
	@Value("${jwt.signing-key:123}")
	private String jwtSigningKey;
	
	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		security.tokenKeyAccess("permitAll()");
		security.checkTokenAccess("isAuthenticated()");
	}


	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception{
		clients.inMemory()
						.withClient(clientId)
						.secret(clientPasswordEncoder.encode(clientSecret))
						//.authorities("ADMIN")
						.authorizedGrantTypes(authorizedGrantTypes)
						.accessTokenValiditySeconds(accessTokenValidititySeconds)
						.refreshTokenValiditySeconds(refreshTokenValiditySeconds)						
						.scopes("read", "write","trust")
						.resourceIds("api");
	}
	
	@Override
	public void configure(final AuthorizationServerEndpointsConfigurer endpoints) {
		endpoints
				.accessTokenConverter(accessTokenConverter())
				.userDetailsService(userService)
				.authenticationManager(authenticationManager);
		
	}
	
	@Bean
	JwtAccessTokenConverter accessTokenConverter() {
		return new JwtAccessTokenConverter();
	}
	
}
