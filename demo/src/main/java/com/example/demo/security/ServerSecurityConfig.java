package com.example.demo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, proxyTargetClass = true)
public class ServerSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private  UserDetailsService userDetailsService;	
	
	@Bean
	public DaoAuthenticationProvider authenticationProvider() {
		DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
		provider.setPasswordEncoder(clientPasswordEncoder());
		provider.setUserDetailsService(userDetailsService);
		return provider;
	}
	
	@Bean("authenticationManagerBean")
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception{
		return super.authenticationManagerBean();
	}
	
	@Bean("clientPasswordEncoder")
	PasswordEncoder clientPasswordEncoder() {
	   return new BCryptPasswordEncoder(4);
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception{
		http
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
			.authorizeRequests()
			.antMatchers("/api/signin/**").permitAll()
            .antMatchers("/api/glee/**").hasAnyAuthority("ADMIN", "USER")
            .antMatchers("/api/users/**").hasAuthority("ADMIN")
            .antMatchers("/api/**").authenticated()
            .anyRequest().authenticated();
//            .and()
//            .exceptionHandling().accessDeniedHandler(new AccessDeniedHandler() {
//				
//				@Override
//				public void handle(HttpServletRequest request, HttpServletResponse response,
//						AccessDeniedException accessDeniedException) throws IOException, ServletException {
//					 //TODO Auto-generated method stub
//					
//				}
//			});
	}
	
	@Autowired      // here is configuration related to spring boot basic authentication
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }
}
