package com.example.demo;

import java.util.Base64;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootTest
class DemoApplicationTests {

	@Test
	void contextLoads() {
	}
	
	@Test
	  void generateBase64EncodedValue() {

	    // Get the Base64 password for appclient:appclient@123
	    //String base64AuthHeader = Base64.getEncoder().encodeToString("secret".getBytes());
		String base64AuthHeader = new BCryptPasswordEncoder().encode("secret");
	    // This Base64 password for appclient:appclient@123 will be used in the http
	    // header when requesting the token
	    System.out.println("appclient's Base64 Password is " + base64AuthHeader);
	  }
}
