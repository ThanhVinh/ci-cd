//package vn.com.receiver;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.kafka.annotation.KafkaListener;
//import org.springframework.kafka.core.KafkaTemplate;
//import org.springframework.stereotype.Component;
//
//@Component
//public class ListenerKafka {
//	
//	@Autowired
//	private KafkaTemplate<String, String> kafkaTemplate;
//	@KafkaListener(topics = "baeldung", groupId = "group-id")
//	public void listenGroupFoo(String message) {
//	    System.out.println("Received Message in group testId: " + message);
//	    kafkaTemplate.send("tasc", "message response from 8084");
//	}
//}
