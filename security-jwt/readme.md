AuthenticationManager : giống như là điều phối, nơi mà chúng ta có thể đăng ký các provider, dưa trên request type. Nó sẽ gửi một yêu cầu xác thực đến đúng nhà cung cấp.

AuthenticationProvider (ex: DaoAuthenticationProvider) : xử lý xác thực. Chỉ gồm 2  chức năng
+ authenticate : thực hiện authentication with the request.
+ supports kiểm tra nếu this provider hỗ trợ loại authentication được chỉ định 

UserDetailsService : Sử dụng để lấy thông tin chi tiết user.

when auth.userDetailsService function call :  will initiate the DaoAuthenticationProvider instance using our implementation of the UserDetailsService interface and register it in the authentication manager.