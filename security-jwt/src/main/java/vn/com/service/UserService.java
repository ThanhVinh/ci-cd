package vn.com.service;

import java.util.Optional;

import vn.com.entity.User;

public interface UserService {
	Optional<User> findByUsername(String username);
}
