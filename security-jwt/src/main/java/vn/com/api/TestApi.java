package vn.com.api;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/api")
public class TestApi {
	
	@GetMapping("/getUsername")
	@PreAuthorize("hasAnyAuthority('USER')")
	public String getUsername() {
		return "hello Vinh";
	}

}
